# -*- coding: utf-8 -*-
"""
Created on Wed Dec 30 12:45:23 2009

@author: francesco.fiduccia@gmail.com -
"""
from base import BasePlugin
import sys

class LoadPlugin(BasePlugin):
    def __init__(self,manager):
        BasePlugin.__init__(self,manager,"load","load an image",'l')
    
    def on_key(self,key):
        if ( key == self.key ):
            self.manager.loadImage(raw_input("enter the file name:"))