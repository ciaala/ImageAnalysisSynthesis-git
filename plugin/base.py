# -*- coding: utf-8 -*-
"""
Created on Tue Dec 29 19:37:46 2009

@author: - francesco.fiduccia@gmail.com
"""

class BasePlugin:
    def __init__(self, manager):
        __init__(self,manager,"base","empty base plugin",'e')
        
    def __init__(self, manager, name, description, key):
        self.manager = manager
        self.name = name
        self.description = description
        self.key = key
        
    def getName(self):
        return self.name
        
    def getDescription(self):    
        return self.description
        
    def key(self):
        return self.key
        
    def on_key(self, key ):
        print "base fallback on_key"
        return
    
    def on_mouse(self, event, x ,y, flags, param ):
        print "base fallback on_mouse"
        return
