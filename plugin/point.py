# -*- coding: utf-8 -*-
from base import BasePlugin
from opencv import highgui
from opencv.cv import *
from random import Random
"""
Created on Tue Dec 29 19:37:46 2009

@author: - francesco.fiduccia@gmail.com
"""

random = Random()

def random_color (random):
    """
    Return a random color
    """
    icolor = random.randint (0, 0xFFFFFF)
    return cvScalar (icolor & 0xff, (icolor >> 8) & 0xff, (icolor >> 16) & 0xff)

class PointPlugin(BasePlugin):
    def __init__(self,manager):
        BasePlugin.__init__(self,manager,"point","draw a point","p")
    
    def on_key( self, key ):
        self.manager.registerMouse(self)
        return

    def on_mouse(self, event, x ,y, flags, param ):        
        if event == highgui.CV_EVENT_LBUTTONDOWN:
            fiename = param
            image =self.manager.getImage(filename)
            pt = cvPoint(x,y)
            cvCircle(image0,pt,10,random_color(random),-1)
            highgui.cvShowImage(filename,image)
            self.manager.unregisterMouse(self)
            return

