# -*- coding: utf-8 -*-
from opencv.cv import *
from opencv import highgui

from document import Document
import point
import quit
import load
"""
Created on Tue Dec 29 21:46:59 2009

@author: - francesco.fiduccia@gmail.com
"""



class PluginManager():
    def __init__(self):
        self.plugins = dict()
        self.descriptions = dict()
        self.documents = dict()
        self.names = dict()
        self.onMouse = list()
        self.loadPlugins()

    def pushPlugin(self, plugin):
        if (not self.plugins.has_key(plugin.key)):
            self.plugins[plugin.key] = plugin
            self.descriptions[plugin.description] = plugin
            self.names[plugin.name] = plugin
            print "plugin " + plugin.name + " loaded ["+ plugin.key + "]\t(" + plugin.description+ ")"
        else:
            print "plugin " + plugin.name + " use the same key"
            
    def on_mouse(self,event, x, y, flags, param):
        for plugin in self.onMouse:
            plugin.on_mouse(event,x,y,flags,param)
        
    def on_key(self):
        message = None
        key = highgui.cvWaitKey()
        while ( 1 ):
            if self.plugins.has_key(key):
                plugin = self.plugins[key]
                message = plugin.on_key(key)
                c = highgui.cvWaitKey()
                
    def on_console(self):
        key = raw_input()
        while (len(self.documents)==0):
            if self.plugins.has_key(key):
                plugin = self.plugins[key]
                message = plugin.on_key(key)
                c = raw_input()
        if (len(self.documents)>=1):
            self.on_key()

    def getPlugins(self):
        return self.plugins
        
    def getNames(self):
        return self.names
        
    def getDescriptions(self):
        return self.descriptions

    def loadPlugins(self):
        self.pushPlugin(point.PointPlugin(self))
        self.pushPlugin(quit.QuitPlugin(self))
        self.pushPlugin(load.LoadPlugin(self))
        
    def register(self, window, image):
        return
    
    def registerMouse(self, plugin):
        self.onMouse.append(plugin)

    def unregisterMouse(self, plugin):
        self.onMouse.remove(plugin)
        
    def loadImage(self, filename):
        document = Document(filename, self);
        self.documents.append(document)
        highgui.cvSetMouseCallback(filename, on_click,[manager,document])
        
    def start(self):
        if (len(self.documents)>=1):
            self.on_key()
        else:
            self.on_console()
        
