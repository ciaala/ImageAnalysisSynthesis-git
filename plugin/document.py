# -*- coding: iso-8859-1 -*-
"""
Created on Wed Dec 30 12:45:23 2009

@author: francesco.fiduccia@gmail.com -
"""
class Document: 
  def __init__(self, filename, manager):
	self.file = filename
	self.manager = manager
	print ">> loading the image file:" + self.file
	self.image = highgui.cvLoadImage(self.file,1)
	if(not self.image):
		print "-- it was not possible to load the image"
		return None
	print "++ the image has been loaded"
	highgui.cvNamedWindow(self.file)
	manager.cvShowImage(self.file,self.image)
    if self.image.width > 640 or self.image.height > 640:
        highgui.cvResizeWindow(self.file,640, 640)

    
