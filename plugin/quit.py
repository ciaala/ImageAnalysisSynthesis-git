# -*- coding: utf-8 -*-
"""
Created on Wed Dec 30 12:45:23 2009

@author: francesco.fiduccia@gmail.com -
"""
from base import BasePlugin
import sys

class QuitPlugin(BasePlugin):
    def __init__(self,manager):
        BasePlugin.__init__(self,manager,"quit","exit the application",'q')
    
    def on_key(self,key):
        if ( key == self.key ):
            sys.exit(0)