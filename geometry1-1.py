#!/usr/bin/python
# -*- coding: utf-8 -*-

from opencv.cv import *
from opencv import highgui

import sys
sys.path.append("./plugin")
from factory import PluginManager
"""
Created on Mon Dec 28 22:38:38 2009

@author: Francesco Fiduccia francesco.fiduccia@gmail.com
"""


if __name__=="__main__" :    
    if len(sys.argv)>1 and (not sys.argv[1] == "--help") and (not sys.argv[1] == "-h") :
        manager = PluginManager()
        img = manager.loadImage(sys.argv[1])
        manager.start()
    elif (len(sys.argv)>1 and ((sys.argv[1] == "--help") or (sys.argv[1] == "-h"))) :
        print "\n\t" + sys.argv[0] + "[OPTIONS] [filename]"
        print "\n\t\tOPTIONS"
        print "\n\t\t-h --help"
    else :
        manager = PluginManager()
        manager.start()
        